import React from 'react';
import ReactDOM from 'react-dom';
import 'src/index.css';
import { ChakraProvider } from '@chakra-ui/react';
import App from 'src/App';

ReactDOM.render(
  <ChakraProvider>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </ChakraProvider>,
  document.getElementById('root'),
);
